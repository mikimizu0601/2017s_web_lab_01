# Herbivorous
* Horse
* Cow
* Sheep
# Carnivorous
* Cat
* Dog
* Polar Bear
* Frog
* Hawk
* Lion
* Fox
# Omnivorous
* Rat
* Capybara
* Raccoon
* Chicken
* Fennec Fox